#!/bin/bash

#PBS -l walltime=0:02:00
#PBS -l nodes=2:ppn=2
#PBS -N Sydorenko_Lab3

cd /home/grid/testbed/tb152/lab3

mpirun -n 4 -quiet "$(pwd)/lab3.out"

sleep 3

cat $(seq 1 4 | sed -n 'p;n') | uniq -c > "stats.txt"
