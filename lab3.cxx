#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>
#include <fstream>
#include <string>
#include <sstream>
#include <unistd.h>
#include<bits/stdc++.h>

using namespace std;

string intToString(int number)
{
	stringstream ss;

	ss << number;

	return ss.str();
}

int main(int argc, char *argv[])
{
    if (argc == 1) {
        int rank, steps, rc;

        MPI_Status status;
        rc = MPI_Init(&argc, &argv);
        rc = MPI_Comm_size(MPI_COMM_WORLD, &steps);
        rc = MPI_Comm_rank(MPI_COMM_WORLD, &rank);

        char processorName[MPI_MAX_PROCESSOR_NAME];
        char prevProcessorName[MPI_MAX_PROCESSOR_NAME];

        int processorNameLength;
        MPI_Get_processor_name(processorName, &processorNameLength);

        if (rank % 2 != 0) {
            rc = MPI_Recv(&prevProcessorName, processorNameLength, MPI_CHAR, rank - 1, 0, MPI_COMM_WORLD, &status);
            stringstream stream;

            stream << processorName << endl;
            stream << prevProcessorName << endl;

            int statsLength = stream.str().length();
            char stats[statsLength + 1];
            strcpy(stats, stream.str().c_str());
            char rankString[1];
            strcpy(rankString, intToString(rank).c_str());

	    char *cmd[] = {"lab3.out", rankString, stats, (char *)0};
            execv(argv[0], cmd);
        } else {
            rc = MPI_Send(&processorName, processorNameLength, MPI_CHAR, rank + 1, 0, MPI_COMM_WORLD);
        }

        rc = MPI_Finalize();
    } else {
        int processRank = atof(argv[1]);
        string stats = argv[2];

        ofstream statsFile;
        statsFile.open(intToString(processRank).c_str(), fstream::out);
        statsFile << stats;

        statsFile.close();
    }

    return 0;
}
