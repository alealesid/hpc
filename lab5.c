#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>

using namespace std;

double function(double x){
    return exp(cos(x));
}

int main(int argc, char *argv[])
{
    int rank, size, tag, rc;

    MPI_Status status;
    rc = MPI_Init(&argc, &argv);
    rc = MPI_Comm_size(MPI_COMM_WORLD, &size);
    rc = MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    tag=7;

    double xMin = atof(argv[1]);
    double xMax = atof(argv[2]);
    int steps = atof(argv[3]);
    double dx = (xMax-xMin) / steps, sum=0, valueForProcess, integral;

    if (rank != 0) {
        valueForProcess = function(xMin + rank * dx);
        rc = MPI_SEND(valueForProcess, 13, MPI_DOUBLE, 0, tag, MPI_COMM_WORLD);
    } else {
        for (int i=1; i < steps; i++) {
            rc = MPI_RECV(valueForProcess, 13, MPI_DOUBLE, i, tag, MPI_COMM_WORLD, &status);
            sum = sum + dx * valueForProcess;
        }

        integral=dx/2.0 * (function(xMin) + function(xMin + steps*dx)) + sum;
        cout<<"The definite integral  is "<<integral<<endl;
    }

    rc = MPI_Finalize();
    return 0;
}
